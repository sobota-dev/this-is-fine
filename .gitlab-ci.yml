image: docker:stable

stages:
  - test
  - build
  - security
  - deploy


### TEST STAGE ###
css_lint:
  stage: test
  image: node:12
  script:
    - npm install stylelint -g
    - stylelint "html/style.css"

docker_lint:
  stage: test
  image: docker:stable
  services:
  - docker:dind
  script:
    - docker run --rm -i hadolint/hadolint < Dockerfile

### SECURITY STAGE ####
sast:
  stage: security
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SAST_VERSION=${SP_VERSION:-$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')}
    - |
      docker run \
        --env SAST_ANALYZER_IMAGES \
        --env SAST_ANALYZER_IMAGE_PREFIX \
        --env SAST_ANALYZER_IMAGE_TAG \
        --env SAST_DEFAULT_ANALYZERS \
        --env SAST_EXCLUDED_PATHS \
        --env SAST_BANDIT_EXCLUDED_PATHS \
        --env SAST_BRAKEMAN_LEVEL \
        --env SAST_GOSEC_LEVEL \
        --env SAST_FLAWFINDER_LEVEL \
        --env SAST_DOCKER_CLIENT_NEGOTIATION_TIMEOUT \
        --env SAST_PULL_ANALYZER_IMAGE_TIMEOUT \
        --env SAST_RUN_ANALYZER_TIMEOUT \
        --volume "$PWD:/code" \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        "registry.gitlab.com/gitlab-org/security-products/sast:$SAST_VERSION" /app/bin/run /code
  dependencies: []
  artifacts:
    reports:
      sast: gl-sast-report.json

container_scanning:
  image: docker:stable
  stage: security
  variables:
    DOCKER_DRIVER: overlay2
    ## Define two new variables based on GitLab's CI/CD predefined variables
    ## https://docs.gitlab.com/ee/ci/variables/README.html#predefined-environment-variables
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
    CLAIR_LOCAL_SCAN_VERSION: v2.0.8_fe9b059d930314b54c78f75afe265955faf4fdc1
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:${CLAIR_LOCAL_SCAN_VERSION}
    - apk add -U wget ca-certificates
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - while( ! wget -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA || true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json

license_management:
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/license-management:$CI_SERVER_VERSION_MAJOR-$CI_SERVER_VERSION_MINOR-stable"
    entrypoint: [""]
  stage: security
  allow_failure: true
  script:
    - /run.sh analyze .
  artifacts:
    reports:
      license_management: gl-license-management-report.json

### BUILD STAGE ###
build_image_feature:
  before_script:
  - type docker >/dev/null 2>&1 && docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY || { echo >&2 "Docker not installed"; }
  stage: build
  services:
    - docker:dind
  script:
    - docker build . -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  except:
    - master
    - tags
  tags:
    - docker

build_image_master:
  before_script:
  - type docker >/dev/null 2>&1 && docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY || { echo >&2 "Docker not installed"; }
  stage: build
  services:
    - docker:dind
  script:
    - docker build . -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  only:
    - master
  tags:
    - docker

build_image_tags:
  before_script:
  - type docker >/dev/null 2>&1 && docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY || { echo >&2 "Docker not installed"; }
  stage: build
  services:
    - docker:dind
  script:
    - docker build . -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  only:
    - tags
  tags:
    - docker

### DEPLOY STAGE ###